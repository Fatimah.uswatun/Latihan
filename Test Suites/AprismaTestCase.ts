<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AprismaTestCase</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-08-02T11:47:34</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6ef07497-d364-49cf-ba74-6a8689280956</testSuiteGuid>
   <testCaseLink>
      <guid>acba986a-7c1b-462a-b84d-6d84be3572ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-1 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e87971e-7cf5-41e1-9f80-71146e6c612f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-2 Login History Report</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>457402c6-e722-4f07-a303-4253596b3f0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC-3 Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
