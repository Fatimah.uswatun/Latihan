<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menu_bank_history_report</name>
   <tag></tag>
   <elementGuidId>1dc3f131-8822-436a-9c10-7b8e36df9831</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'BO_LOGIN_HISTORY_REPORT' and @ref_element = 'Object Repository/Login/frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Login History Report']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Login/frame</value>
   </webElementProperties>
</WebElementEntity>
