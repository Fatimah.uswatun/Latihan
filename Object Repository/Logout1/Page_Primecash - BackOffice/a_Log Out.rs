<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Log Out</name>
   <tag></tag>
   <elementGuidId>b5a93a0f-d999-4c59-af42-463ab477c8f0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/backoffice/login/logout</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log Out</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;top&quot;)/div[@class=&quot;navbar navbar-fixed-top&quot;]/div[@class=&quot;navbar-inner&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;navbar-text pull-right open&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[1]/a[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Logout1/Page_Primecash - BackOffice/iframe_login</value>
   </webElementProperties>
</WebElementEntity>
